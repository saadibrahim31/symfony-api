<?php

declare(strict_types=1);

/**
 * There is a code supporting calculation if a car is damaged.
 *
 * Now it should be extended to support "calculating if a painting of car's exterior" is damaged (this means,
 * if a painting of any of "car details" is not OK - for example a "door is scratched").
 *
 * paint is damage
 * Class CarDetail
 */
abstract class CarDetail {

    /**
     * @var bool
     */
    protected $isBroken;

    /**
     * @var bool
     */
    protected $isPaintingDamaged;

    /**
     * CarDetail constructor.
     * @param bool $isBroken
     * @param bool $isPaintingDamaged
     */
    public function __construct(bool $isBroken = false, bool $isPaintingDamaged = false)
    {
        $this->isBroken = $isBroken;
        $this->isPaintingDamaged = $isPaintingDamaged;
    }

    abstract function isBroken();

    abstract function isPaintingDamaged();
}

class Door extends CarDetail
{
    public function isBroken(): bool
    {
        return $this->isBroken;
    }

    public function isPaintingDamaged(): bool
    {
        return $this->isPaintingDamaged;
    }
}

class Tyre extends CarDetail
{
    public function isBroken(): bool
    {
        return $this->isBroken;
    }

    public function isPaintingDamaged(): bool
    {
        return $this->isPaintingDamaged;
    }
}

/**
 * Class Car
 */
class Car
{

    /**
     * @var CarDetail[]
     */
    private $details;

    /**
     * @param CarDetail[] $details
     */
    public function __construct(array $details)
    {
        $this->details = $details;
    }

    public function isBroken(): bool
    {
        foreach ($this->details as $detail) {

            if ($detail->isBroken()) {
                return true;
            }
        }

        return false;
    }

    public function isPaintingDamaged(): bool
    {
        foreach ($this->details as $detail) {

            if ($detail->isPaintingDamaged()) {
                return true;
            }
        }

        return false;
    }
}

$car = new Car(
    [
        new Door(false, false),
        new Tyre(false, true)// i think tyre can have damage painting, if not then different interface and isPaintingDamaged is not abstract anymore
    ]); // we pass a list of all details
var_dump($car->isPaintingDamaged());
var_dump($car->isBroken());
class base
{

}

class child extends base
{

}

interface testn {
    public function func(child $param);
}



abstract class test
{
    abstract public function func();
}


class test2 extends test implements testn
{
    public function func(child $param){

    }
}


new test2();