<?php

namespace App\DataFixtures;

use App\Entity\BlogPost;
use App\Entity\Comment;
use App\Entity\User;
use App\Security\TokenGenerator;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class AppFixtures
 * @package App\DataFixtures
 */
class AppFixtures extends Fixture
{
    private CONST USERS = [
        [
            "username" => "12_12",
            "password" => "secret123#",
            "fullname" => "saad elzway",
            "email" => "12_12@mail.com",
            "roles" => [User::ROLE_SUPER_ADMIN],
            "enabled" => true,
        ],
        [
            "username" => "13_13",
            "password" => "secret123#",
            "fullname" => "saad elzway",
            "email" => "13_13@mail.com",
            "roles" => [User::ROLE_ADMIN],
            "enabled" => true,
        ],
        [
            "username" => "14_14",
            "password" => "secret123#",
            "fullname" => "saad elzway",
            "email" => "14_14@mail.com",
            "roles" => [User::ROLE_WRITER],
            "enabled" => true,
        ],
        [
            "username" => "15_15",
            "password" => "secret123#",
            "fullname" => "saad elzway",
            "email" => "15_15@mail.com",
            "roles" => [User::ROLE_COMMENTATOR],
            "enabled" => false,
        ],
        [
            "username" => "16_16",
            "password" => "secret123#",
            "fullname" => "saad elzway",
            "email" => "16_16@mail.com",
            "roles" => [User::ROLE_EDITOR],
            "enabled" => true,
        ],
    ];

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var \Faker\Factory
     */
    private $faker;

    /**
     * @var TokenGenerator
     */
    private $tokenGenerator;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param TokenGenerator $tokenGenerator
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, TokenGenerator $tokenGenerator)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->faker = \Faker\Factory::create();
        $this->tokenGenerator = $tokenGenerator;
    }

    public function load(ObjectManager $manager)
    {
        try {
            $this->loadUser($manager);
            $this->loadBlogPost($manager);
            $this->loadComment($manager);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
    }

    /**
     * @param ObjectManager $manager
     */
    private function loadUser(ObjectManager $manager)
    {
        foreach (self::USERS as $userFixture) {
            $user = new User();
            $user->setEmail($userFixture["email"]);
            $user->setFullname($userFixture["fullname"]);
            $user->setUsername($userFixture["username"]);
            $user->setPassword($this->passwordEncoder->encodePassword($user, $userFixture["password"]));
            $user->setRoles($userFixture["roles"]);
            $user->setEnabled($userFixture["enabled"]);
            if (!$userFixture["enabled"]) {
                $user->setConfirmationToken($this->tokenGenerator->getRandomSecureToken());
            }
            $this->addReference("user_" . $userFixture["username"], $user);

            $manager->persist($user);
        }
        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    private function loadBlogPost(ObjectManager $manager)
    {
        for ($i = 0; $i < 100; $i++) {
            $blogPost = new BlogPost();
            $auther = $this->getRandomUserRefrence($blogPost);
            $blogPost->setName($this->faker->realText(30));
            $blogPost->setAuther($auther);
            $blogPost->setText($this->faker->realText());
            $blogPost->setSlug($this->faker->slug);
            $blogPost->setCreated($this->faker->dateTimeThisYear);
            $this->addReference("blog_post_$i", $blogPost);

            $manager->persist($blogPost);

        }
        $manager->flush();
    }

    private function getRandomUserRefrence($entity): UserInterface
    {
        $randomUser = self::USERS[rand(0, 4)];
        if ($entity instanceof BlogPost && !$this->canWritePost($randomUser)) {
            $this->getRandomUserRefrence($entity);
        }
        if ($entity instanceof Comment && !$this->canWriteComment($randomUser)) {
            $this->getRandomUserRefrence($entity);
        }

        return $this->getReference("user_" . $randomUser["username"]);
    }

    /**
     * @param array $randomUser
     * @return bool
     */
    private function canWritePost(array $randomUser): bool
    {
        return count(array_intersect($randomUser["roles"], [User::ROLE_SUPER_ADMIN, User::ROLE_WRITER, User::ROLE_ADMIN])) > 0;
    }

    /**
     * @param array $randomUser
     * @return bool
     */
    private function canWriteComment(array $randomUser): bool
    {
        return count(array_intersect($randomUser["roles"], [User::ROLE_SUPER_ADMIN, User::ROLE_WRITER, User::ROLE_ADMIN, User::ROLE_COMMENTATOR])) > 0;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    private function loadComment(ObjectManager $manager)
    {
        for ($i = 0; $i < 100; $i++) {
            $comment = new Comment();
            $comment->setAuther($this->getRandomUserRefrence($comment));
            $comment->setBlogPost($this->getReference("blog_post_$i"));
            $comment->setContent($this->faker->realText());
            $comment->setPublished($this->faker->dateTimeThisYear);
            $manager->persist($comment);
        }

        $manager->flush();
    }
}
