<?php

declare(strict_types=1);

namespace App\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\AutheredEntityInterface;
use App\Entity\BlogPost;
use App\Entity\Comment;
use App\Entity\EntityInterface;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class AuthoredEntitySubscriber
 * @package App\EventSubscriber
 */
class AuthoredEntitySubscriber implements EventSubscriberInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * AuthoredEntitySubscriber constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ["addAuthenticatedUser", EventPriorities::PRE_WRITE]
        ];
    }

    /**
     * @param ViewEvent $event
     */
    public function addAuthenticatedUser(ViewEvent $event)
    {
        $entity = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        if (!$this->isValid($entity, $method)) {
            return;
        }

        if (null === $this->tokenStorage->getToken()) {
            return false;
        }

        /** @var UserInterface $user */
        $user = $this->tokenStorage->getToken()->getUser();
        $entity->setAuther($user);
    }

    /**
     * @param mixed $entity
     * @param string $method
     * @return bool
     */
    private function isValid($entity, string $method): bool
    {
        if (!$entity instanceof AutheredEntityInterface || $method !== Request::METHOD_POST) {
            return false;
        }

        return true;
    }
}