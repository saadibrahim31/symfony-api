<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\UserConfirmation;
use App\Exception\InvalidConfirmationTokenException;
use App\Security\UserConfirmationService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class UserConfirmationSubscriber
 * @package App\EventSubscriber
 */
class UserConfirmationSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserConfirmationService
     */
    private $userConfirmationService;

    public function __construct(
        UserConfirmationService $userConfirmationService
    ) {
        $this->userConfirmationService = $userConfirmationService;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                'confirmUser',
                EventPriorities::POST_VALIDATE,
            ],
        ];
    }

    public function confirmUser(ViewEvent $event)
    {
        $request = $event->getRequest();

        // you can get this from debug:router
        if ('api_user_confirmations_post_collection' !==
            $request->get('_route')) {
            return;
        }

        /** @var UserConfirmation $confirmationToken */
        $confirmationToken = $event->getControllerResult();

        try {
            $this->userConfirmationService->confirmUser(
                $confirmationToken->confirmationToken
            );
        } catch (InvalidConfirmationTokenException $e) {
            //var_dump( $e->getMessage());
            throw  $e;
        }

        $event->setResponse(new JsonResponse(null, Response::HTTP_OK));
    }
}