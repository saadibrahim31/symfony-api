<?php

declare(strict_types=1);

namespace App\Serializer;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

/**
 * Class UserAttributeNormalizer
 * @package App\Serializer
 */
class UserAttributeNormalizer implements ContextAwareNormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;
    CONST USER_ATTRIBUTE_ALREADY_NORMALIZER_ALREADY_CALLED = "USER_ATTRIBUTE_ALREADY_NORMALIZER_ALREADY_CALLED";

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * covert object to array first
     * see notebad image
     *
     * @inheritDoc
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        if ($this->isUserHimself($object)) {
            $context["groups"][] = "get-owner";
        }

        return $this->passOn($object, $format, $context);
    }

    /**
     * @param $object
     * @return bool
     */
    private function isUserHimself($object): bool
    {
        if ($object->getUsername() === $this->tokenStorage->getToken()->getUser()->getUsername()) {
            return true;
        }

        return false;
    }

    /**
     * @param $object
     * @param string $format
     * @param array $context
     * @return array|\ArrayObject|bool|float|int|string|null
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    private function passOn($object, string $format, array $context)
    {
        if (!$this->serializer instanceof NormalizerInterface) {
            throw new \LogicException(
                sprintf(
                    "Cannot normalize object %s cause injected serializer is not a normalizer",
                    $object
                ));
        }
        $context[self::USER_ATTRIBUTE_ALREADY_NORMALIZER_ALREADY_CALLED] = true;
        return $this->serializer->normalize($object, $format, $context);
    }

    /**
     * @inheritDoc
     *
     * @param mixed $data
     * @param string|null $format
     * @param array $context
     * @return bool
     */
    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        if (isset($context[self::USER_ATTRIBUTE_ALREADY_NORMALIZER_ALREADY_CALLED])) {
            return false;
        }

        return $data instanceof User;
    }
}