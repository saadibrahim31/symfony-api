<?php

declare(strict_types=1);

namespace App\Controller;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class ResetPasswordAction
 * @package App\Controller
 */
class ResetPasswordAction
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __constructs(ValidatorInterface $validator)
    {
        var_dump($validator);
        // with invoke this does not work
        $this->validator = $validator;
    }

    public function __invoke(
        User $userEntity,
        ValidatorInterface $validator,
        UserPasswordEncoderInterface $userPasswordEncoder,
        EntityManagerInterface $entityManager,
        JWTTokenManagerInterface $tokenManager
    )
    {
        // $resetPass = new ResetPasswordAction()
        // $resetPass()
        // invoke help you to call the class directly
        //var_dump($data->getNewPassword(), $data->getNewRetypedPassword(), $data->getOldPassword());
        //var_dump($data->getNewPassword(), $data->getNewRetypedPassword(), $data->getOldPassword());
        //exit;

        $validator->validate($userEntity);

        $userEntity->setPassword($userPasswordEncoder->encodePassword(
            $userEntity, $userEntity->getNewPassword()
        ));

        // after password changed old token are still valid
        $userEntity->setPasswordChangeDate(time());
        $entityManager->flush();

        $token = $tokenManager->create($userEntity);
        return new JsonResponse(["token" => $token]);
        // validator is only called after we return data from this action.
        // only here is to check the user current password but we have just modified it.
        // entity is persisted automatically, if it pass validation
    }
}