<?php

declare(strict_types=1);

namespace App\Controller;


use App\Security\UserConfirmationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ExampleController
 * @package App\Controller
 * @Route("/example")
 */
class ExampleController extends AbstractController
{
    /**
     * @Route("/", name="blog_list")

    public function keka()
    {
        return new JsonResponse(["Here:www" => ""]);
    }*/

    /**
     * @Route("/", name="default_index")
     */
    public function index()
    {
        return $this->render(
            'base.html.twig'
        );
    }

    /**
     * @Route("/post/{id}", methods={"DELETE"})
     * @return JsonResponse
     */
    public function remove()
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($object);
        $em->flush();
        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/confirm-user/{token}", name="default_confirm_token")
     * @throws \App\Exception\InvalidConfirmationTokenException
     */
    public function confirmUser(
        string $token,
        UserConfirmationService $userConfirmationService
    ) {
        $userConfirmationService->confirmUser($token);

        return $this->redirectToRoute('default_index');
    }
}
