<?php

declare(strict_types=1);

namespace App\Exception;

use Throwable;

/**
 * Class EmptyBodyException
 */
class EmptyBodyException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct("The body of POST/PUT can not be empty", $code, $previous);
    }
}