<?php

declare(strict_types=1);

namespace App\Entity;

/**
 * Interface PublishedDateEntityInterface
 * @package App\Entity
 */
interface PublishedDateEntityInterface
{
    /**
     * @param \DateTimeInterface $created
     * @return PublishedDateEntityInterface
     */
    public function setCreated(\DateTimeInterface $created): PublishedDateEntityInterface;

}