<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CommentRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CommentRepository::class)
 *
 * @ApiResource(
 *     attributes={
 *         "order"={"published": "DESC"},
 *         "pagination_client_enabled"=true,
 *         "pagination_client_items_per_page"=true
 *     },
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={
 *                  "groups"={"get","read"}
 *              },
 *          },
 *          "put"={
 *              "security"="is_granted('ROLE_EDITOR') or (is_granted('ROLE_COMMENTATOR') and object.getAuther() === user)",
 *              "normalization_context"={
 *                  "groups"={"get","read"}
 *              },
 *              "denormalization_context"={
 *                  "groups"={"put"}
 *              },
 *           }
 *     },
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "security"="is_granted('ROLE_COMMENTATOR')",
 *               "normalization_context"={
 *                  "groups"={"get","read"}
 *              },
 *          },
 *          "api_blog_posts_comments_get_subresource"={
 *              "normalization_context"={
 *                  "groups"={"get-comment-with-auther"}
 *             },
 *          },
 *     },
 *     denormalizationContext={
 *          "groups"={"post"}
 *     }
 * )
 */
class Comment implements AutheredEntityInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get-comment-with-auther"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read", "post", "get-comment-with-auther"})
     * @Assert\Length(min="10", max="300")
     * @Assert\NotBlank()
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read","get-comment-with-auther"})
     */
    private $published;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read", "get-comment-with-auther"})
     */
    private $auther;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BlogPost", inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read", "get-comment-with-auther"})
     */
    private $blogPost;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getPublished(): ?\DateTimeInterface
    {
        return $this->published;
    }

    public function setPublished(\DateTimeInterface $published): self
    {
        $this->published = $published;

        return $this;
    }

    /**
     * @return UserInterface
     */
    public function getAuther(): UserInterface
    {
        return $this->auther;
    }

    public function setAuther(UserInterface $auther): AutheredEntityInterface
    {
        $this->auther = $auther;

        return $this;
    }

    /**
     * @return null|BlogPost
     */
    public function getBlogPost(): ?BlogPost
    {
        return $this->blogPost;
    }

    /**
     * @param BlogPost $blogPost
     * @return Comment
     */
    public function setBlogPost(BlogPost $blogPost)
    {
        $this->blogPost = $blogPost;

        return $this;
    }

    public function __toString()
    {
        return $this->content;
    }
}
