<?php

declare(strict_types=1);

namespace App\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Interface EntityInterface
 * @package App\Entity
 */
interface AutheredEntityInterface
{
    public function setAuther(UserInterface $auther): AutheredEntityInterface;
}