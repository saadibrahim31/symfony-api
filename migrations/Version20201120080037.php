<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201120080037 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_BA5AE01DBCC5EBBA');
        $this->addSql('CREATE TEMPORARY TABLE __temp__blog_post AS SELECT id, auther_id, name, text, created, slug, published FROM blog_post');
        $this->addSql('DROP TABLE blog_post');
        $this->addSql('CREATE TABLE blog_post (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, auther_id INTEGER NOT NULL, name VARCHAR(30) DEFAULT NULL COLLATE BINARY, created DATETIME NOT NULL, slug VARCHAR(255) NOT NULL COLLATE BINARY, published DATETIME NOT NULL, text CLOB DEFAULT NULL, CONSTRAINT FK_BA5AE01DBCC5EBBA FOREIGN KEY (auther_id) REFERENCES user (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO blog_post (id, auther_id, name, text, created, slug, published) SELECT id, auther_id, name, text, created, slug, published FROM __temp__blog_post');
        $this->addSql('DROP TABLE __temp__blog_post');
        $this->addSql('CREATE INDEX IDX_BA5AE01DBCC5EBBA ON blog_post (auther_id)');
        $this->addSql('DROP INDEX IDX_9474526CBCC5EBBA');
        $this->addSql('DROP INDEX IDX_9474526CA77FBEAF');
        $this->addSql('CREATE TEMPORARY TABLE __temp__comment AS SELECT id, auther_id, blog_post_id, content, published FROM comment');
        $this->addSql('DROP TABLE comment');
        $this->addSql('CREATE TABLE comment (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, auther_id INTEGER NOT NULL, blog_post_id INTEGER NOT NULL, content VARCHAR(255) DEFAULT NULL COLLATE BINARY, published DATETIME NOT NULL, CONSTRAINT FK_9474526CBCC5EBBA FOREIGN KEY (auther_id) REFERENCES user (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_9474526CA77FBEAF FOREIGN KEY (blog_post_id) REFERENCES blog_post (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO comment (id, auther_id, blog_post_id, content, published) SELECT id, auther_id, blog_post_id, content, published FROM __temp__comment');
        $this->addSql('DROP TABLE __temp__comment');
        $this->addSql('CREATE INDEX IDX_9474526CBCC5EBBA ON comment (auther_id)');
        $this->addSql('CREATE INDEX IDX_9474526CA77FBEAF ON comment (blog_post_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_BA5AE01DBCC5EBBA');
        $this->addSql('CREATE TEMPORARY TABLE __temp__blog_post AS SELECT id, auther_id, name, text, created, slug, published FROM blog_post');
        $this->addSql('DROP TABLE blog_post');
        $this->addSql('CREATE TABLE blog_post (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, auther_id INTEGER NOT NULL, name VARCHAR(30) DEFAULT NULL, created DATETIME NOT NULL, slug VARCHAR(255) NOT NULL, published DATETIME NOT NULL, text VARCHAR(255) DEFAULT NULL COLLATE BINARY)');
        $this->addSql('INSERT INTO blog_post (id, auther_id, name, text, created, slug, published) SELECT id, auther_id, name, text, created, slug, published FROM __temp__blog_post');
        $this->addSql('DROP TABLE __temp__blog_post');
        $this->addSql('CREATE INDEX IDX_BA5AE01DBCC5EBBA ON blog_post (auther_id)');
        $this->addSql('DROP INDEX IDX_9474526CBCC5EBBA');
        $this->addSql('DROP INDEX IDX_9474526CA77FBEAF');
        $this->addSql('CREATE TEMPORARY TABLE __temp__comment AS SELECT id, auther_id, blog_post_id, content, published FROM comment');
        $this->addSql('DROP TABLE comment');
        $this->addSql('CREATE TABLE comment (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, auther_id INTEGER NOT NULL, blog_post_id INTEGER NOT NULL, content VARCHAR(255) DEFAULT NULL, published DATETIME NOT NULL)');
        $this->addSql('INSERT INTO comment (id, auther_id, blog_post_id, content, published) SELECT id, auther_id, blog_post_id, content, published FROM __temp__comment');
        $this->addSql('DROP TABLE __temp__comment');
        $this->addSql('CREATE INDEX IDX_9474526CBCC5EBBA ON comment (auther_id)');
        $this->addSql('CREATE INDEX IDX_9474526CA77FBEAF ON comment (blog_post_id)');
    }
}
