<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201102110051 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE comment (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, auther_id INTEGER NOT NULL, content VARCHAR(255) DEFAULT NULL, published DATETIME NOT NULL)');
        $this->addSql('CREATE INDEX IDX_9474526CBCC5EBBA ON comment (auther_id)');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username VARCHAR(255) DEFAULT NULL, password VARCHAR(255) DEFAULT NULL, fullname VARCHAR(255) DEFAULT NULL, email VARCHAR(100) DEFAULT NULL, created DATETIME NOT NULL)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__blog_post AS SELECT id, name, text, created FROM blog_post');
        $this->addSql('DROP TABLE blog_post');
        $this->addSql('CREATE TABLE blog_post (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, auther_id INTEGER NOT NULL, name VARCHAR(30) DEFAULT NULL COLLATE BINARY, text VARCHAR(255) DEFAULT NULL COLLATE BINARY, created DATETIME NOT NULL, slug VARCHAR(255) NOT NULL, CONSTRAINT FK_BA5AE01DBCC5EBBA FOREIGN KEY (auther_id) REFERENCES user (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO blog_post (id, name, text, created) SELECT id, name, text, created FROM __temp__blog_post');
        $this->addSql('DROP TABLE __temp__blog_post');
        $this->addSql('CREATE INDEX IDX_BA5AE01DBCC5EBBA ON blog_post (auther_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP INDEX IDX_BA5AE01DBCC5EBBA');
        $this->addSql('CREATE TEMPORARY TABLE __temp__blog_post AS SELECT id, name, text, created FROM blog_post');
        $this->addSql('DROP TABLE blog_post');
        $this->addSql('CREATE TABLE blog_post (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(30) DEFAULT NULL, text VARCHAR(255) DEFAULT NULL, created DATETIME NOT NULL)');
        $this->addSql('INSERT INTO blog_post (id, name, text, created) SELECT id, name, text, created FROM __temp__blog_post');
        $this->addSql('DROP TABLE __temp__blog_post');
    }
}
