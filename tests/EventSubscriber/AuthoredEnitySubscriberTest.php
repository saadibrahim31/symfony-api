<?php

declare(strict_types=1);

namespace App\Tests\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\BlogPost;
use App\Entity\User;
use App\EventSubscriber\AuthoredEntitySubscriber;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use App\Entity\Comment;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Class AuthoredEntitySubscriberTest
 * @package App\Tests\EventSubscriber
 */
class AuthoredEntitySubscriberTest extends TestCase
{
    public function testConfiguration()
    {
        $result = AuthoredEntitySubscriber::getSubscribedEvents();

        $this->assertArrayHasKey(KernelEvents::VIEW, $result);
        $this->assertEquals(
            ['addAuthenticatedUser', EventPriorities::PRE_WRITE],
            $result[KernelEvents::VIEW]
        );
    }

    public function testSetAuthorCall()
    {
        // interface or abstract
        $tokenMock = $this->getMockBuilder(TokenInterface::class)->getMockForAbstractClass();
        //$tokenMock->expects($this->exactly(2))
        //$tokenMock->expects($this->never())
        $tokenMock->expects($this->once())
            ->method("getUser")
            ->willReturn(new User());
        $tokenStorageMock = $this->getMockBuilder(TokenStorageInterface::class)->getMockForAbstractClass();
        $tokenStorageMock->expects($this->once())
            ->method("getToken")
            ->willReturn($tokenMock);

        $blogPost =  $this->getMockBuilder(BlogPost::class)->getMock();
        $blogPost->expects($this->exactly(1))
        ->method('setAuther');
        $kernel =  $this->getMockBuilder(HttpKernelInterface::class)->getMockForAbstractClass();
        $request =  $this->getMockBuilder(Request::class)->getMockForAbstractClass();
        $event = new ViewEvent($kernel, $request, 1, $blogPost);
        $event->getRequest()->setMethod("POST");
        (new AuthoredEntitySubscriber($tokenStorageMock))->addAuthenticatedUser($event);
    }

    public function testAuthorNotCall()
    {
        // interface or abstract
        $tokenMock = $this->getMockBuilder(TokenInterface::class)->getMockForAbstractClass();
        //$tokenMock->expects($this->exactly(2))
        //$tokenMock->expects($this->never())
        $tokenMock->expects($this->never())
            ->method("getUser")
            ->willReturn(new User());
        $tokenStorageMock = $this->getMockBuilder(TokenStorageInterface::class)->getMockForAbstractClass();
        $tokenStorageMock->expects($this->never())
            ->method("getToken")
            ->willReturn($tokenMock);

        $blogPost =  $this->getMockBuilder(BlogPost::class)->getMock();
        $blogPost->expects($this->exactly(0))
            ->method('setAuther');
        $kernel =  $this->getMockBuilder(HttpKernelInterface::class)->getMockForAbstractClass();
        $request =  $this->getMockBuilder(Request::class)->getMockForAbstractClass();
        $event = new ViewEvent($kernel, $request, 1, $blogPost);
        $event->getRequest()->setMethod("GET");
        (new AuthoredEntitySubscriber($tokenStorageMock))->addAuthenticatedUser($event);
    }

    public function testConfigurationTwo()
    {
        $result = AuthoredEntitySubscriber::getSubscribedEvents();

        $this->assertArrayHasKey(KernelEvents::VIEW, $result);
        $this->assertEquals(
            ['addAuthenticatedUser', EventPriorities::PRE_WRITE],
            $result[KernelEvents::VIEW]
        );
    }

    /**
     * @@dataProvider providerSetAuthorCall
     */
    public function testSetAuthorCallTwo(string $className, bool $shouldCallSetAuthor, string $method)
    {
        $entityMock = $this->getEntityMock($className, $shouldCallSetAuthor);
        $tokenStorageMock = $this->getTokenStorageMock();
        $eventMock = $this->getEventMock($method, $entityMock);

        (new AuthoredEntitySubscriber($tokenStorageMock))->addAuthenticatedUser(
            $eventMock
        );
    }

    public function testNoTokenPresent()
    {
        $tokenStorageMock = $this->getTokenStorageMock(false);
        $eventMock = $this->getEventMock('POST', new class {});

        (new AuthoredEntitySubscriber($tokenStorageMock))->addAuthenticatedUser(
            $eventMock
        );
    }

    public function providerSetAuthorCall(): array
    {
        return [
            [BlogPost::class, true, 'POST'],
            [BlogPost::class, false, 'GET'],
            ['NonExisting', false, 'POST'],
            [Comment::class, true, 'POST']
        ];
    }

    /**
     * @return MockObject|TokenStorageInterface
     */
    private function getTokenStorageMock(bool $hasToken = true): MockObject
    {
        $tokenMock = $this->getMockBuilder(TokenInterface::class)
            ->getMockForAbstractClass();
        $tokenMock->expects($hasToken ? $this->once() : $this->never())
            ->method('getUser')
            ->willReturn(new User());

        $tokenStorageMock = $this->getMockBuilder(TokenStorageInterface::class)
            ->getMockForAbstractClass();
        $tokenStorageMock->expects($this->once())
            ->method('getToken')
            ->willReturn($hasToken ? $tokenMock : null);

        return $tokenStorageMock;
    }

    /**
     * @return MockObject|GetResponseForControllerResultEvent
     */
    private function getEventMock(string $method, $controllerResult): MockObject
    {
        $requestMock = $this->getMockBuilder(Request::class)
            ->getMock();
        $requestMock->expects($this->once())
            ->method('getMethod')
            ->willReturn($method);

        $eventMock =
            $this->getMockBuilder(GetResponseForControllerResultEvent::class)
                ->disableOriginalConstructor()
                ->getMock();

        $eventMock->expects($this->once())
            ->method('getControllerResult')
            ->willReturn($controllerResult);
        $eventMock->expects($this->once())
            ->method('getRequest')
            ->willReturn($requestMock);

        return $eventMock;
    }

    /**
     * @return MockObject
     */
    private function getEntityMock(string $className, bool $shouldCallSetAuthor): MockObject
    {
        $entityMock = $this->getMockBuilder($className)
            ->setMethods(['setAuthor'])
            ->getMock();
        $entityMock->expects($shouldCallSetAuthor ? $this->once() : $this->never())
            ->method('setAuthor');

        return $entityMock;
    }
}

/**
 *
 * Example to test final calsses
 *
 * Class ViewEventMock
 * @package App\Tests\EventSubscriber
 */
class ViewEventMock
{
    protected $viewEvent;

    public function __construct()
    {
        $this->viewEvent = new \ReflectionClass(ViewEvent::class);
    }

    /**
     * @param $name
     * @param $args
     * @return mixed
     */
    public function __call($name, $args)
    {
        $bar = $this->viewEvent->newInstance($args);
        return $bar->$name();
    }
}